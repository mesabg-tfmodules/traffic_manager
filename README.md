# Bucket CDN Module

This module is capable to generate an S3 behind a CDN cloudfront distribution.
NOTE: Certificate MUST be always present on us-east-1 region.

Module Input Variables
----------------------

- `environment` - environment name
- `name` - general CDN name
- `domain` - base domain
- `subdomain` - subdomain name
- `subdirectory` - subdirectory name (default "")
- `viewer_protocol_policy` - protocol policy (default redirect-to-https)
- `cache_enabled` - enable or not cache enabled (default false)
- `cache_behaviors` - multiple path match cache behaviors

Usage
-----

```hcl
module "traffic" {
  source        = "git::https://gitlab.com/mesabg-tfmodules/traffic-manager.git"

  environment   = "environment"

  name          = "cdn name"

  domain        = "foo.com"
  subdomain     = "bar.foo.com"

  applications  = [
    {
      path    = "/some"
      domain  = "some.domain.extra"    
    },
  ]
}
```

Outputs
=======

 - `s3` - Created S3 bucket
 - `cdn` - Cloudfront distribution
 - `record` - Route53 record


Authors
=======
##### Moisés Berenguer <moises.berenguer@gmail.com>
