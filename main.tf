terraform {
  required_version = ">= 0.13.2"
}

data "aws_route53_zone" "zone" {
  name = var.domain
}

data "aws_acm_certificate" "certificate" {
  domain      = var.domain
  types       = ["AMAZON_ISSUED"]
  statuses    = ["ISSUED"]
  most_recent = true
}

resource "aws_cloudfront_distribution" "cloudfront_distribution" {

  // Creating default origin with custom domain name
  origin {
    domain_name = var.default_domain
    origin_id   = "Custom-${var.default_domain}"

    custom_origin_config {
      http_port               = 80
      https_port              = 443
      origin_protocol_policy  = var.default_https ? "https-only" : "http-only"
      origin_ssl_protocols    = ["TLSv1", "TLSv1.1", "TLSv1.2"]
    }
  }

  // Default cache behavior for normal custom origin configuration
  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "Custom-${var.default_domain}"

    forwarded_values {
      query_string = true
      headers      = ["*"]

      cookies {
        forward = "all"
      }
    }

    lambda_function_association {
      event_type   = "origin-request"
      lambda_arn   = var.lambda_arn
      include_body = true
    }

    min_ttl                = 0
    default_ttl            = 0
    max_ttl                = 0
    compress               = false
    viewer_protocol_policy = "redirect-to-https"
  }

  // Custom origins for each application
  dynamic "origin" {
    for_each = var.applications
    iterator = application

    content {
      domain_name = application.value.domain
      origin_id   = "Custom-${application.value.domain}"

      custom_origin_config {
        http_port               = 80
        https_port              = 443
        origin_protocol_policy  = application.value.https ? "https-only" : "http-only"
        origin_ssl_protocols    = ["TLSv1", "TLSv1.1", "TLSv1.2"]
      }
    }
  }

  // Ordered cache behaviors for applications sub urls "/*"
  dynamic "ordered_cache_behavior" {
    for_each = var.applications
    iterator = application

    content {
      path_pattern     = "${application.value.path}*"
      allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
      cached_methods   = ["GET", "HEAD"]
      target_origin_id = "Custom-${application.value.domain}"

      forwarded_values {
        query_string = true
        headers      = ["*"]

        cookies {
          forward = "all"
        }
      }

      lambda_function_association {
        event_type   = "origin-request"
        lambda_arn   = var.lambda_arn
        include_body = true
      }

      min_ttl                = 0
      default_ttl            = 0
      max_ttl                = 0
      compress               = false
      viewer_protocol_policy = "redirect-to-https"
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = var.name
  default_root_object = ""
  http_version        = "http2"
  wait_for_deployment = true

  aliases             = [var.subdomain]

  price_class         = "PriceClass_All"

  viewer_certificate {
    acm_certificate_arn             = data.aws_acm_certificate.certificate.arn
    cloudfront_default_certificate  = false
    minimum_protocol_version        = "TLSv1.1_2016"
    ssl_support_method              = "sni-only"
  }

  restrictions {
    geo_restriction {
      restriction_type      = "none"
    }
  }

  tags = {
    Name        = var.name
    Environment = var.environment
  }
}

resource "aws_route53_record" "route53_record" {
  zone_id                   = data.aws_route53_zone.zone.zone_id
  name                      = var.subdomain
  type                      = "A"

  alias {
    name                    = aws_cloudfront_distribution.cloudfront_distribution.domain_name
    zone_id                 = aws_cloudfront_distribution.cloudfront_distribution.hosted_zone_id
    evaluate_target_health  = false
  }
}
