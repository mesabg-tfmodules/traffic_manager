variable "environment" {
  type        = string
  description = "Environment name"
}

variable "name" {
  type        = string
  description = "CDN name"
}

variable "domain" {
  type        = string
  description = "Domain name"
}

variable "subdomain" {
  type        = string
  description = "Subdomain name"
}

variable "default_domain" {
  type        = string
  description = "Default custom domain for root"
}

variable "default_https" {
  type        = string
  description = "Apply or not https configuration to default domain"
  default     = false
}

variable "applications" {
  type = list(object({
    path      = string
    domain    = string
    https     = bool
  }))
  description = "Custom Cache Behavior"
  default     = []
}

variable "lambda_arn" {
  type        = string
  description = "Lambda function ARN"
}
